class CategoriesController < ApplicationController
    def show 
        @posts = Post
        cat_id = Category.find_by_param(params[:url])
        unless cat_id.nil?
            @posts = Post.paginate(page: params[:page]).where(status: 1).where(category_id: cat_id.id).order("created_at DESC")
            render "posts/index"
        else
            render :status => 404
        end
    end
end
