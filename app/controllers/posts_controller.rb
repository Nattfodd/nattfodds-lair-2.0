class PostsController < ApplicationController

  before_filter :find_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:new, :edit, :update, :destroy]
  before_filter :user_is_admin, only: [:new, :edit, :update, :destroy]
  before_filter :get_categories

  def index 
    @posts = Post
    @posts = Post.paginate(page: params[:page]).where(status: 1).order("created_at DESC")
  end

  def new
    @post = Post.new
  end

  def create 
    @post = Post.new(post_params)
    @post.user = current_user
    if params[:publish]
      @post.status = 1
      @post.was_published = 1
    else 
      @post.status = 0
    end
    @post.save
    if @post.errors.empty?
      redirect_to post_path(@post)
    else
      render "new"
    end
  end

  def show
    if @post.status == 1
      unless user_signed_in? and @post.user_id == current_user.id 
        @post.increment!(:views)
      end
    else
      unless user_signed_in? and @post.user_id == current_user.id
        render_404
      end
    end
  end

  # /posts/1/edit GET
  def edit 
  end

  # /post/1 PUT
  def update
    @post.update_attributes(post_params)
    if params[:publish]
      if @post.was_published == 0     
        @post.created_at = DateTime.now 
        @post.was_published = 1
      end
      @post.status = 1
    else
      @post.status = 0
    end
    @post.save

    if @post.errors.empty?
      redirect_to post_path(@post)
    else
      if params[:publish]
        render "edit"
      else 
        redirect_to "index"
      end
    end  
  end

  def destroy
    @post.destroy
    redirect_to action: "index"
  end

  def draft_list
    @posts = Post
    @posts = Post.paginate(page: params[:page]).where(status: 0, user_id: current_user.id).order("created_at DESC")
    render "index"
  end

private

    def post_params
      params.require(:post).permit(:title, :body, :category_id)
    end

    def find_post
      @post = Post.find_by_id(params[:id])
      if @post.nil? 
        render_404
      end
    end 

    def user_is_admin 
      redirect_to action: "index" unless current_user.role == 1
    end

    def get_categories 
      @cat = Category.find(:all)
    end

end
