module ApplicationHelper
  def before_cut(text)
    cutted_text = text.split('[cut]', 2)
    return cutted_text[0]
  end

  def delete_cut(text)
    text.gsub!(/\[cut\]/, "")
    return text
  end

  def has_cut?(text)
    text.index(/\[cut\]/)
  end

end
