class Category < ActiveRecord::Base
    validates_uniqueness_of :url

    def to_param
        url
    end

    def self.find_by_param(input)
        find_by_url(input)
    end
end
