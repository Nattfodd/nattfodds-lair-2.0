# To run:
# rails runner converter/fill_categories.rb
#
# IT
# Music
# Movies
# Books
# Events
# Politics
# About

c = Array.new
c[0] = Category.new(name: "IT", url: "it")
c[1] = Category.new(name: "Music", url: "music")
c[2] = Category.new(name: "Movies", url: "movies")
c[3] = Category.new(name: "Books", url: "books")
c[4] = Category.new(name: "Events", url: "events")
c[5] = Category.new(name: "Politics", url: "politics")
c[6] = Category.new(name: "About me", url: "about")

c.each do |c| 
  c.save
end