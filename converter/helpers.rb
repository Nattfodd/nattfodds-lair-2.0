include ActionView::Helpers

def post_format(body)
  body = simple_format(body, {}, {:sanitize => false})
  body = body.gsub(/\<!--more--\>/, "[cut]")
  body = body.gsub(/\<h2 style=\"text-align: justify;\"\>/, "<h1>")
  body = body.gsub(/\<\/h2\>/, "</h1>")
  # remove [caption] tag
  body = body.gsub(/\[caption.*caption=\"(?<title>.*)\"\](?<image>.*)\[\/caption\]/, "<p>\\k<title></p>\\k<image>")
  # remove [spoiler] tag
  body = body.gsub(/\[spoiler.+?\]/, "")
  body = body.gsub(/\[\/spoiler\]/, "")
  body = body.gsub(/\<br \/\>/, "")
end