require File.expand_path('../models_old', __FILE__)
require File.expand_path('../helpers', __FILE__)

old_posts = PostOld.where(post_status: "publish")

old_posts.each do |p| 
  # post_title -> :title
  # post_content -> :body
  # post_date_gmt -> :created_at
  # status -> "1"
  # was_published -> "1"
  # user_id -> "1"
  post = Post.new
  post.title = p.post_title
  post.body = post_format(p.post_content)
  post.created_at = p.post_date_gmt
  post.status = 1
  post.was_published = 1
  post.user_id = 1
  post.views = PostMeta.where(post_id: p.id, meta_key: :views).first.meta_value
  post.save
  if post.errors.empty?
    puts "Post \"#{post.title}\" was converted successfully"
  else 
    puts post.errors
  end
end