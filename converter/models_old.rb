class OldDatabase < ActiveRecord::Base

  self.abstract_class = true
  establish_connection :old_database

end

class PostOld < OldDatabase 

  self.table_name = "wp2_posts"

end

class PostMeta < OldDatabase

  self.table_name = "wp2_postmeta"

end