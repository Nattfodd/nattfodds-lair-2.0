class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.belongs_to  :user
      t.text        :title
      t.text        :body
      t.integer     :views, default: 0
      t.integer     :status
      t.timestamps
    end
  end
end