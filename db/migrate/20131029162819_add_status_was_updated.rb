class AddStatusWasUpdated < ActiveRecord::Migration
  def change
    add_column :posts, :was_published, :integer, default: 0
  end
end
