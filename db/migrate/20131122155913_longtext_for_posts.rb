class LongtextForPosts < ActiveRecord::Migration
  def change
    change_column :posts, :body, :text, :limit => 4294967295
  end
end
